@Tag4
Feature: AUT  login


Scenario: To verify login for valid data
Given Login page is displayed
When User enters login detail
Then Home page is displayed
And verify projectName
And verify startDate
And verify endDate
And verify customerName
And verify businessUnit
And verify shadowAllocationAllowed
And verify allocationsforMonth
And Verify pending approvals