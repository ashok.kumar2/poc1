Feature: Resource Allocation

@Tag8
Scenario: To verify login for valid data

Given Dashboard page is displayed
When Supervisor change request
Then Success message
And Current Requests
And Request Made By
And Request Made By Value
And Request Status
And Request Status Value
And Current Supervisor
And Current Supervisor Value
And New Supervisor
And New Supervisor Value
And Effective Date
And Effective Date Value
And Approval Details
And Action
And Close
And Disable