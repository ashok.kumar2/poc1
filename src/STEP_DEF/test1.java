package STEP_DEF;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class test1 {
	static WebDriver dr;
	WebDriverWait wait1;
	WebDriverWait drw;
	WebElement we;
	  int actual =1;
	  int expected=1;
	  int counter=1;
	
		public ArrayList<WebElement> al,al2;
		public String s="";
		 private SoftAssert sa = new SoftAssert();
	  
	
	
	private static final Boolean F = null;

static ExtentHtmlReporter reporter = new ExtentHtmlReporter("./ExtentReport/extent.html");  
static ExtentReports extent = new ExtentReports();


public static void extent_report(String test_case_name, String info, String pass, String fail , String act, String exp) {

extent.attachReporter(reporter);
 
ExtentTest logger = extent.createTest(test_case_name);
 
logger.log(Status.INFO, info);
 
if(act.equals(exp)) {
 logger.log(Status.PASS, pass);}  
 else
 logger.log(Status.FAIL, fail);
 
extent.flush();
}
	@Given("^login page$")
	public void login_page() throws Throwable {
		
		
		
		System.out.println("Login page is displayed");  

		System.setProperty("webdriver.chrome.driver","chromedriver_78.exe");
		dr=new ChromeDriver();
		dr.get("https://ra-staging.globallogic.com/login");
	  
	}

@When("^on entering valid login crediatials and click sign in button$")
	public void on_entering_valid_login_crediatials_and_click_sign_in_button() throws Throwable {
	   dr.findElement(By.name("username")).sendKeys("ashok.kumar2");
	  // dr.findElement(By.id(id));
	   dr.findElement(By.name("password")).sendKeys("samrat@123");
	   //dr.findElement(By.className("btn btn-primary btn-block btn-lg")).click();
	   dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();
	}

	@Then("^Home page is displyed$")
	public void home_page_is_displyed() throws Throwable {
		System.out.println("Home page displyed");
		
	}
	
	
	

	
	
	
	
	
	@Then("^Home page should contain Logged Pending Approvals$")
	public void home_page_should_contain_Logged_Pending_Approvals() throws Throwable {
	
		
		//ul[@id='profile-image-container']/li/a/b
		
		String appr="Pending Approvals";

		WebDriverWait wait=new WebDriverWait(dr,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'Pending Approvals')]")));
		String appr1=dr.findElement(By.xpath("//h4[contains(.,'Pending Approvals')]")).getText();
		
		SoftAssert as=new SoftAssert();
		as.assertEquals(appr,appr1);
		System.out.println(appr1);
		as.assertAll();
		
		
		
		
		
	}
	


	@Then("^Then Home page should contain Home$")
	public void then_Home_page_should_contain_Home() throws Throwable {

		   String Home="Home";
		  // String Home1=dr.findElement(By.xpath("/html/body/user-app/app-header/div/div/div/div[2]/ul[1]/li[1]/a/text()")).getText();
		 //  dr.findElement(By.linkText("/home")).click();
		   WebDriverWait wait=new WebDriverWait(dr,6);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Home")));
		   String Home1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[1]/a")).getText();
		  System.out.println(Home1);
		   SoftAssert as =new SoftAssert();
		   as.assertEquals(Home, Home1);
		   as.assertAll();
	}

@Then("^Then Home page should contain Requests$")
public void then_Home_page_should_contain_Requests() throws Throwable {
	 WebDriverWait wait=new WebDriverWait(dr,6);
	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("dropdown-toggle")));
	String req1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/a")).getText();
	SoftAssert as=new SoftAssert();
	String req="Requests";
	as.assertEquals(req,req1);
	System.out.println(req1);
	as.assertAll();
	
}

@Then("^Then Home page should contain Reports$")
public void then_Home_page_should_contain_Reports() throws Throwable {

	WebDriverWait wait=new WebDriverWait(dr,6);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("dropbtn")));
	
	String rep1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[3]/a")).getText();
	
	SoftAssert as=new SoftAssert();
	String rep="Reports";
	as.assertEquals(rep1,rep);
	System.out.println(rep1);
	as.assertAll();

}

@Then("^Then Home page should contain Feedback$")
public void then_Home_page_should_contain_Feedback() throws Throwable {

	String li="https://docs.google.com/forms/d/e/1FAIpQLSeSlnfGt4oGlE3T-LXEr0w2R4a1EWmrGrdpYkthPuRpFTiNhQ/viewform";
		WebDriverWait wait=new WebDriverWait(dr,6);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("FeedBack")));
	
		String feed1=dr.findElement(By.linkText("FeedBack")).getText();
		
		
		SoftAssert as=new SoftAssert();
		String feed="FeedBack";
		as.assertEquals(feed1,feed);
		System.out.println(feed1);
		as.assertAll();
}

@Then("^Then Home page should contain Logged User Name$")
public void then_Home_page_should_contain_Logged_User_Name() throws Throwable {

	
	
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='profile-image-container']/li/a/b")));
	
	String log_name1=dr.findElement(By.xpath("//ul[@id='profile-image-container']/li/a/b")).getText();
	
	SoftAssert as=new SoftAssert();
	String log_name="ASHOK KUMAR";
	as.assertEquals(true,dr.findElement(By.xpath("//ul[@id='profile-image-container']/li/a/b")).isDisplayed());
	System.out.println(log_name1);
	as.assertAll();
	
}

@Then("^Then Home page should contain Logged Pending Approvals$")
public void then_Home_page_should_contain_Logged_Pending_Approvals() throws Throwable {
	
	//ul[@id='profile-image-container']/li/a/b
	
	String appr="Pending Approvals";

	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'Pending Approvals')]")));
	String appr1=dr.findElement(By.xpath("//h4[contains(.,'Pending Approvals')]")).getText();
	
	SoftAssert as=new SoftAssert();
	as.assertEquals(appr,appr1);
	System.out.println(appr1);
	as.assertAll();
	
}

@Then("^Then Home page should contain Logged My Submissions$")
public void then_Home_page_should_contain_Logged_My_Submissions() throws Throwable {

	String my_sub="My Submissions";
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'My Submissions')]")));
	String my_sub1=dr.findElement(By.xpath("//h4[contains(.,'My Submissions')]")).getText();
	
	SoftAssert as=new SoftAssert();
	as.assertEquals(my_sub1,my_sub);
	System.out.println(my_sub1);
	as.assertAll();
}

@Then("^Then Home page should contain Logged My Historical Actions$")
public void then_Home_page_should_contain_Logged_My_Historical_Actions() throws Throwable {


	String my_His="My Historical Actions";
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'My Historical Actions')]")));
	String my_His1=dr.findElement(By.xpath("//h4[contains(.,'My Historical Actions')]")).getText();
	
	SoftAssert as=new SoftAssert();
	as.assertEquals(my_His1,my_His);
	System.out.println(my_His1);
	as.assertAll();
	
}

@Then("^Then Home page should contain Logged My Projects$")
public void then_Home_page_should_contain_Logged_My_Projects() throws Throwable {
	String my_pro="My Projects";
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'My Proje')]")));
	String my_pro1=dr.findElement(By.xpath("//h4[contains(.,'My Proje')]")).getText();
	SoftAssert as=new SoftAssert();
	as.assertEquals(my_pro,my_pro1);
	System.out.println(my_pro);
	as.assertAll();
}


@When("^on entering valid login crediatials and click sign in button and click on drop down and add  start and end date and click filter$")
public void on_entering_valid_login_crediatials_and_click_sign_in_button_and_click_on_drop_down_and_add_start_and_end_date_and_click_filter() throws Throwable {
	 
	   WebDriverWait wait=new WebDriverWait(dr,6);
	 dr.findElement(By.name("username")).sendKeys("arun.pp");
	   dr.findElement(By.name("password")).sendKeys("Amritha@1234");
	   //dr.findElement(By.className("btn btn-primary btn-block btn-lg")).click();
	   dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Impersonation')]")));
	   dr.findElement(By.xpath("//a[contains(text(),'Impersonation')]")).click();
	   
	   
wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mysearch")));
	   
	   
	   dr.findElement(By.id("mysearch")).sendKeys("1261");
	   
	   dr.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
		dr.findElement(By.xpath("(//a[contains(@href, '#')])[3]")).click();
	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".mt-5 .caret")));
	   
	   
dr.findElement(By.cssSelector(".mt-5 .caret")).click();
	

  
   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='mat-input-0']")));
   
   dr.findElement(By.xpath("//input[@id='mat-input-0']")).click();
   dr.findElement(By.xpath("//input[@id='mat-input-0']")).clear();
   
  
dr.findElement(By.xpath("//input[@id='mat-input-0']")).sendKeys("2017-10-21");
	


   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='mat-input-1']")));
   
  dr.findElement(By.xpath("//input[@id='mat-input-1']")).click();
   dr.findElement(By.xpath("//input[@id='mat-input-1']")).clear();
dr.findElement(By.xpath("//input[@id='mat-input-1']")).sendKeys("2019-11-27");
	dr.findElement(By.xpath("//input[@value='Apply Filter']")).click();
}

@Then("^Result set is displyed in Awaiting Allocations Approvals$")
public void result_set_is_displyed_in_Awaiting_Allocations_Approvals() throws Throwable {
	System.out.println("in then 1");
	
	
	try {
		System.out.println("in try");
		
		 String d1="2017-10-21";
		   String d2="2019-11-27";
		  /* SimpleDateFormat  fo=new SimpleDateFormat("dd MM yyyy");
		   Date dd1=fo.format(d1);
		   Date dd2=fo.format(d2);*/
		   
		   java.util.Date dd1=new SimpleDateFormat("yyyy-MM-dd").parse(d1);
		   java.util.Date dd2=new SimpleDateFormat("yyyy-MM-dd").parse(d2);
	dr.findElement(By.xpath("//div[@class='ag-center-cols-container']"));
	System.out.println("befor sddate");
	String sdate=dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div/div[7]")).getText();
	System.out.println("afr sddate");
	System.out.println(sdate);
	long date1=dd2.getTime()-dd1.getTime();
	System.out.println("*********");
	System.out.println(date1);
	
	System.out.println(TimeUnit.DAYS.convert(date1,TimeUnit.MILLISECONDS));
	
	java.util.Date dd3=new SimpleDateFormat("yyyy-MM-dd").parse(sdate);
	
	long date2=dd3.getTime()-dd1.getTime();
	
	System.out.println(TimeUnit.DAYS.convert(date2,TimeUnit.MILLISECONDS));
	
	if(date2<=date2) {
	SoftAssert as=new SoftAssert();
	as.assertEquals(true, true);
	as.assertAll();
		
	}
	else
	{
		SoftAssert as=new SoftAssert();
		as.assertEquals(true, false);
		as.assertAll();
	}
	}
	catch(Exception e) {
		SoftAssert as=new SoftAssert();
		as.assertEquals(true, true);
		as.assertAll();
		
	}
	   
}

@Then("^count of set should displyed in Awaiting Allocations Approvals$")
public void count_of_set_should_displyed_in_Awaiting_Allocations_Approvals() throws Throwable {
 boolean a=true;
	//h4[@class='pull-left']
 SoftAssert as=new SoftAssert();
	try {
	String s=dr.findElement(By.xpath("//h4[@class='pull-left']")).getText();
	System.out.println(s);
	String s1=s.substring(31,32);
	System.out.println(s1);
	int x=Integer.parseInt(s1);
	as.assertEquals(true,dr.findElement(By.xpath("//h4[@class='pull-left']")).isDisplayed());
	System.out.println("Count of pending approvals:"+x);
	
	as.assertAll();
	
	//System.out.println("Count of pending approvals:"+x);
	
	
	
	}
	catch(Exception e) {
		//SoftAssert as=new SoftAssert();
		as.assertEquals(true, false);
		as.assertAll();
	}
	
	
	


}

@Then("^in case of no  pending reports mesage should dispalyed$")
public void in_case_of_no_pending_reports_mesage_should_dispalyed() throws Throwable {
	/*   WebDriverWait wait=new WebDriverWait(dr,6);
	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert alert-danger")));
	   
	   
	String s=dr.findElement(By.className("alert alert-danger")).getText();
	System.out.println(s);*/
	try {
	System.out.println("in pending");
	String s=dr.findElement(By.xpath("//strong[contains(text(),'No approvals are pending')]")).getText();
	boolean x=false;
	if(dr.findElement(By.xpath("//strong[contains(text(),'No approvals are pending')]")).isDisplayed());
	{
		
		SoftAssert as=new SoftAssert();
		as.assertEquals(s,"No approvals are pending");
		as.assertAll();
	}
	}catch(Exception e){
		SoftAssert as=new SoftAssert();
		as.assertEquals(true,true);
		as.assertAll();
		
	}
	
	
}



//testcase 3


@Given("^Dashboard page is displayed$")

public void dashboard_page_is_displayed() throws Throwable {  
System.out.println("^Login page is displayed$");

System.setProperty("webdriver.chrome.driver","chromedriver_v78.exe");
dr = new ChromeDriver();
dr.get("https://ra-staging.globallogic.com/login");

System.out.println("^User enters login details$");

dr.findElement(By.xpath("//div[@class='input-group']//input[@name='username']")).sendKeys("rajat.agrawal");
dr.findElement(By.xpath("//div[@class='input-group']//input[@name='password']")).sendKeys("mittallogic@21");
dr.findElement(By.xpath("//input[@class='btn btn-primary btn-block btn-lg']")).click();
 
System.out.println("^Home page is displayed$");  

dr.manage().window().maximize();

try
{Thread.sleep(9000);}
catch(InterruptedException e)
{ e.printStackTrace();}
}




@When("^Employee assign request$")
public void employee_assign()throws Throwable {

System.out.println("dusra feature file chla h be");

         WebElement we =dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/a"));

Actions kb = new Actions(dr);
kb.moveToElement(we).build().perform();

dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[1]/a")).click();

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[1]/ul/li[2]/a")));

dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[1]/ul/li[2]/a")).click();

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='right-inner-addon']//input")));

dr.findElement(By.xpath("//div[@class='right-inner-addon']//input")).sendKeys("325100");

try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}

kb.sendKeys(Keys.ENTER).build().perform();

try
{Thread.sleep(4000);}
catch(InterruptedException e)
{ e.printStackTrace();}

kb.sendKeys(Keys.PAGE_DOWN).build().perform();

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='aggrid']/div/div[2]/div/div[3]/div[2]/div/div/div/div[@col-id='Project']//input")));

dr.findElement(By.xpath("//*[@id='aggrid']/div/div[2]/div/div[3]/div[2]/div/div/div/div[@col-id='Project']//input")).sendKeys("11003482");

 try
{Thread.sleep(4000);}
catch(InterruptedException e)
{ e.printStackTrace();}

         kb.sendKeys(Keys.ENTER).build().perform();

        /* wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='ASSIGNMENT_STATUS']//input")));*/

         try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}
         
dr.findElement(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='ASSIGNMENT_STATUS']//input")).sendKeys("Rotation");

try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}

 kb.sendKeys(Keys.ENTER).build().perform();

 wait1 = new WebDriverWait(dr,20);
 wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='PERCENTAGE_ALLOCATION']//input")));

 
dr.findElement(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='PERCENTAGE_ALLOCATION']//input")).sendKeys("100");
     

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='BILLING_ROLE']//input")));


dr.findElement(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='BILLING_ROLE']//input")).sendKeys("Data Manager");

try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}

         kb.sendKeys(Keys.ENTER).build().perform();

try
{Thread.sleep(2000);}
catch(InterruptedException e)
{ e.printStackTrace();}

Select s = new Select(dr.findElement(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='ASSIGN_TYPE']//select")));
s.selectByVisibleText("Onsite");

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='LOCATION']//input")));


dr.findElement(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='LOCATION']//input")).sendKeys("Noida");


try
{Thread.sleep(2000);}
catch(InterruptedException e)
{ e.printStackTrace();}

kb.sendKeys(Keys.ENTER).build().perform();

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='Start_Date']//input")));


dr.findElement(By.xpath("//*[@id='aggrid']/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[@col-id='Start_Date']//input")).sendKeys("2019-11-28");

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='tabA']/div/form/div/div[2]/div[2]/button")));


// dr.findElement(By.xpath("//div[@class='ag-center-cols-container']/div[1]/div[@comp-id='274']//input")).sendKeys("Kuch b lelo");

dr.findElement(By.xpath("//*[@id='tabA']/div/form/div/div[2]/div[2]/button")).click();

/*wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='clearfix form-actions mb-20']/div[1]/div[1]/button[2]")));*/

try
{Thread.sleep(9000);}
catch(InterruptedException e)
{ e.printStackTrace();}

dr.findElement(By.xpath("//div[@class='clearfix form-actions mb-20']/div[1]/div[1]/button[2]")).click();

/* wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='clearfix form-actions mb-20']/div[1]/div[1]/button[@class= 'btn btn-default']")));*/

try
{Thread.sleep(9000);}
catch(InterruptedException e)
{ e.printStackTrace();}

dr.findElement(By.xpath("//div[@class='clearfix form-actions mb-20']/div[1]/div[1]/button[@class= 'btn btn-default']")).click();

wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='navbar']/div[1]/div[2]/ul[1]/li[4]/a")));


//dr.findElement(By.xpath("//div[@id='navbar']/div[1]/div[2]/ul[1]/li[4]/a")).click();
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Impersonation')]")));
Thread.sleep(50000);

dr.findElement(By.xpath("//a[contains(text(),'Impersonation')]")).click();



wait1 = new WebDriverWait(dr,20);
wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='right-inner-addon']//input")));


dr.findElement(By.xpath("//div[@class='right-inner-addon']//input")).sendKeys("1261");

try
{Thread.sleep(4000);}
catch(InterruptedException e)
{ e.printStackTrace();}

kb.sendKeys(Keys.ENTER).build().perform();

try
{Thread.sleep(4000);}
catch(InterruptedException e)
{ e.printStackTrace();}



}





	
@Then("^Approver page verify$")
public void Approver_page()throws Throwable {

//WebDriverWait wait = new WebDriverWait(dr,20);

String act = dr.findElement(By.xpath("//ng-component/div/div[2]/div/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/span/a")).getText();

String exp = "1";

SoftAssert sa = new SoftAssert();
 sa.assertEquals(act, exp);
 sa.assertAll();
 
 try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}

}

@Then("^Click Button$")
public void click_button()throws Throwable{
String act = "";

WebElement element = dr.findElement(By.xpath("//ng-component/div/div[2]/div/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/span/a"));
if (element.isDisplayed() && element.isEnabled()) {
   act = "True";
}
String exp = "True";

SoftAssert sa = new SoftAssert();
 sa.assertEquals(act, exp);
 sa.assertAll();
 
 try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}

}

@Then("^Approve$")
public void approve()throws Throwable{

String act = dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[1]/div[2]/div/div/div[9]/div[2]/div/span[1]")).getText();
System.out.println(act);

String exp = "Approve";

SoftAssert sa = new SoftAssert();
 sa.assertEquals(act, exp);
 sa.assertAll();
 
 try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}
}

@Then("^Reject$")
public void reject()throws Throwable{

String act = dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[1]/div[2]/div/div/div[10]/div[2]/div/span[1]")).getText();
System.out.println(act);

String exp = "Reject";

SoftAssert sa = new SoftAssert();
 sa.assertEquals(act, exp);
 sa.assertAll();
 
 try
{Thread.sleep(3000);}
catch(InterruptedException e)
{ e.printStackTrace();}
 
 dr.close();

}

	
	
	
	
	
	
	
	//testcase4




@Given("^Login page is displayed$")
public void login_page_is_displayed() throws Throwable {

 System.setProperty("webdriver.chrome.driver", "chromedriver_v78.exe");
 dr =new ChromeDriver();
 dr.manage().window().maximize();

dr.get("https://ra-staging.globallogic.com/login");
System.out.println("Login page is displayed");

}

@When("^User enters login detail$")
public void user_enters_login_details() throws Throwable {
System.out.println("User enters login details");

drw=new WebDriverWait(dr,20);
drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")));
dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")).sendKeys("meghna.majhi");
dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[6]/div/input")).sendKeys("30041997Megh#");
dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();

}

@Then("^Home page is displayed$")
public void home_page_is_displayed() throws Throwable {

dr.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
WebElement we1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/a"));
Actions kb=new Actions(dr);
kb.moveToElement(we1).build().perform();
WebElement we2=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[1]/a"));
Actions kb1=new Actions(dr);
kb1.moveToElement(we2).build().perform();

dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[1]/ul/li[1]/a")).click();
dr.findElement(By.xpath("//*[@id=\"mysearch\"]")).sendKeys("LogRhythm");
dr.findElement(By.xpath("//*[@id=\"imaginary_container\"]/div/type-ahead/typeahead-container/ul/li[1]/a")).click();

System.out.println("Home page is displayed");
}
@Then("^verify projectName$")
public void verifyprojectName() throws Throwable {
String projectName="Project Type: Time and Material";
String projectName1=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[1]")).getText();
//System.out.println(projectName1);

SoftAssert sa1=new SoftAssert();
sa1.assertEquals(projectName,projectName1);
sa1.assertAll();

}
@Then("^verify startDate$")
public void verifyStartDate() throws Throwable {
String startDate="Start Date: 2012-11-05";
String startDate1=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[2]")).getText();
//System.out.println(startDate1);

SoftAssert sa2=new SoftAssert();
sa2.assertEquals(startDate,startDate1);
sa2.assertAll();

}
@Then("^verify endDate$")
public void verifyEndDate() throws Throwable {
String endDate="End Date: 2020-06-28";
String endDate1=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[3]")).getText();
//System.out.println(endDate1);

SoftAssert sa3=new SoftAssert();
sa3.assertEquals(endDate,endDate1);
sa3.assertAll();

}
@Then("^verify customerName$")
public void verifyCustomerName() throws Throwable {
String customerName="Customer Name: LogRhythm";
String customerName1=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[4]")).getText();
//System.out.println(customerName1);

SoftAssert sa4=new SoftAssert();
sa4.assertEquals(customerName,customerName1);
sa4.assertAll();
}
@Then("^verify businessUnit$")
public void verifyBusinessUnit() throws Throwable {
String businessUnit="Business Unit: Technology";
String businessUnit1=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[5]")).getText();
//System.out.println(businessUnit1);

SoftAssert sa5=new SoftAssert();
sa5.assertEquals(businessUnit,businessUnit1);
sa5.assertAll();

}
@Then("^verify shadowAllocationAllowed$")
public void verifyShadowAllocationAllowed() throws Throwable {
String shadowAllocationAllowed="Shadow Allocation Allowed: No";
String shadowAllocationAllowed1=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[6]")).getText();
//System.out.println(shadowAllocationAllowed1);

SoftAssert sa6=new SoftAssert();
sa6.assertEquals(shadowAllocationAllowed,shadowAllocationAllowed1);
sa6.assertAll();

}
@Then("^verify allocationsforMonth$")
public void verifyAllocationsforMonth() throws Throwable {
String allocationsforMonth="Nov, 2019";
//dr.manage().window().maximize();
 dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[7]/span/fdms-month-picker/div/span")).click();
 try {
 Thread.sleep(3000);
 }
 catch(Exception e)
 {}
 String allocationsforMonth1=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[7]/span/fdms-month-picker/div/div/div[1]")).getText();
 //System.out.println(s);

SoftAssert sa7=new SoftAssert();
sa7.assertEquals(allocationsforMonth,allocationsforMonth1);
sa7.assertAll();
dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div/div[1]/div[1]/div/div[2]/div/p[7]/span/fdms-month-picker/div/div/div[4]/span")).click();

}
@Then("^Verify pending approvals$")
public void VerifyPendingApprovals() throws Throwable{
dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/alert/div/strong[2]/a")).click();
dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div/div[10]/button-renderer/div/button")).click();
String er="Approval Details for - Amrit Pal Singh (308594)";

System.out.println(er);
try {
 Thread.sleep(3000);
 }
 catch(Exception e)
 {}
String ar=dr.findElement(By.xpath("/html/body/modal-container[2]/div/div/modal-content/div/div[1]/h4")).getText();
System.out.println(ar);
if(er.equals(ar)) {
System.out.println("Successful");
}
else {
System.out.println("Unsuccessful");
}
//SoftAssert sa =new SoftAssert();
//sa.assertEquals(er,ar);
//sa.assertAll();
dr.findElement(By.xpath("/html/body/modal-container[2]/div/div/modal-content/div/div[2]/div/div[2]/div/div/div/div/button")).click();
try {
 Thread.sleep(3000);
 }
 catch(Exception e)
 {}
dr.findElement(By.xpath("/html/body/modal-container/div/div/ng-component/div/form/div[1]/button/span")).click();

}


//testcase5


@Given("^Login Page is displayed$")
public void login_page_is_displayed1() throws Throwable {
	
	System.setProperty("webdriver.chrome.driver", "chromedriver_78.exe");
	dr=new ChromeDriver();
	dr.get("https://ra-staging.globallogic.com/login");
  
}

@When("^User enters the details$")
public void user_enter_login_details() throws Throwable {
   
	dr.manage().window().maximize();
	 drw=new WebDriverWait(dr,20);
	 drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")));
	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")).sendKeys("rajat.kumar2");
	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[6]/div/input")).sendKeys("Programming@853");
	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();

	Actions ac=new Actions(dr);
       drw=new WebDriverWait(dr,20);
	   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ramenu']/li[2]/a")));
       we=dr.findElement(By.xpath("//*[@id='ramenu']/li[2]/a"));
       ac.moveToElement(we).build().perform();
       
       we=dr.findElement(By.xpath("//*[@id='ramenu']/li[2]/ul/li[1]/a"));
       ac.moveToElement(we).build().perform();
       
       we=dr.findElement(By.xpath("//*[@id='ramenu']/li[2]/ul/li[1]/ul/li[2]/a"));
       ac.moveToElement(we).click().build().perform();
       

	   drw=new WebDriverWait(dr,20);
	   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mysearch\"]")));
	   we=dr.findElement(By.xpath("//*[@id=\"mysearch\"]"));
	   we.sendKeys("325154");
	   drw=new WebDriverWait(dr,20);
	   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"imaginary_container\"]/div/type-ahead/typeahead-container/ul/li/a")));
	   we.sendKeys(Keys.ENTER);
}

@Then("^Employee Profile Photo$")
public void then_Employee_Profile_Photo() throws Throwable {

	try {
		Thread.sleep(3000);
	}
	catch(Exception e) {
		
	}
	WebElement ImageFile = dr.findElement(By.xpath("//div[@class='text-center']/img"));
	    Boolean ImagePresent = (Boolean)((JavascriptExecutor)dr).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
	    
	    SoftAssert sa=new SoftAssert();
	 sa.assertEquals(true, ImagePresent);
	 sa.assertAll();
	 
	  
}	

 @Then("^Then Employee Name$")
 public void then_Employee_Name() throws Throwable {
 
	 drw=new WebDriverWait(dr,20);
	   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[1]/div/p[1]/b")));
	 
	   String s=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[1]/div/p[1]/b")).getText();
	 
	 SoftAssert sa=new SoftAssert();
	 sa.assertEquals("SARTHAK GOEL (325154)", s);
	 sa.assertAll();
	 
 }

 @Then("^Then Employee Designation$")
 public void then_Employee_Designation() throws Throwable {

	   drw=new WebDriverWait(dr,20);
	   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[1]/div/span/b")));
	   
	 String s=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[1]/div/span/b")).getText();
	
	 SoftAssert sa=new SoftAssert();
	 sa.assertEquals("Trainee Software Engineer", s);
	 sa.assertAll();
	 
 }

   @Then("^Then Employee Joining Date$")
 public void then_Employee_Joining_Date() throws Throwable {
	   
	   drw=new WebDriverWait(dr,20);
	   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[1]/div/p[2]/span/b")));


	   String s=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[1]/div/p[2]/span/b")).getText();
  	 
	   SoftAssert sa=new SoftAssert();
		 sa.assertEquals("2019-10-09", s);
		 sa.assertAll();
}

  @Then("^Then Employee Supervisor$")
 public void then_Employee_Supervisor() throws Throwable {
	  
	  drw=new WebDriverWait(dr,20);
	   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[2]/span[1]/p/b")));

	  
	  String s=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div[1]/div/div[2]/span[1]/p/b")).getText();    	  
	
	  SoftAssert sa=new SoftAssert();
		 sa.assertEquals("Sarita Rani (316557)", s);
		 sa.assertAll();
}
	
  
  
  //test6
  
  
  @When("^Employee enter employeewise page details$")
  public void employee_enter_employeewise_page_details() throws Throwable {
		dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")).sendKeys("ashok.kumar2");
	  	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[6]/div/input")).sendKeys("samrat@123");
	  	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();
	  	try {
	  		Thread t=new Thread();
	  		t.sleep(7000);
	  	}
	      catch(Exception e)
	  	{}

  	 WebElement we1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/a"));
  	    Actions ac=new Actions(dr);
  	    ac.moveToElement(we1).build().perform();
  	    WebElement we2=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[1]/a"));
  	    ac.moveToElement(we2).build().perform();
  	    dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[1]/ul/li[2]/a")).click();
  	    
  	    try {
  			Thread.sleep(7000);
  		}
  		catch(Exception e)
  		{}
  		dr.findElement(By.xpath("//*[@id=\"mysearch\"]")).sendKeys("325106");
  		//Actions ac=new Actions(dr);
  		try {
  			Thread.sleep(7000);
  		}
  		catch(Exception e)
  		{}
  		ac.sendKeys(Keys.ENTER).build().perform();
  		try {
  			Thread.sleep(7000);
  		}
  		catch(Exception e)
  		{}
  		Actions ac1=new Actions(dr);
  		ac1.sendKeys(Keys.PAGE_DOWN).build().perform();
  		
  }

  @Then("^verify add row button$")
  public void verify_add_row_button() throws Throwable {
  	
  	try {
  		Thread.sleep(7000);
  	}
  	catch(Exception e)
  	{}
  	dr.findElement(By.xpath("//button-renderer/div/button/i")).click();
  	counter++;
  	try {
  		Thread.sleep(7000);
  	}
  	catch(Exception e)
  	{}
  try {
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div["+counter+"]"));
  	actual++;
  	}
  	catch(Exception e)
  	{}
  	expected++; 
  	
  	SoftAssert sa=new SoftAssert();
  	sa.assertEquals(actual, expected);
  	sa.assertAll(); 
  }

  @Then("^Then able to navigate to next window$")
  public void then_able_to_navigate_to_next_window() throws Throwable {
  	dr.get("https://ra-staging.globallogic.com/allocation/empwiseallocation");
  	try {
  		Thread.sleep(7000);
  	}
  	catch(Exception e)
  	{}
  	dr.findElement(By.xpath("//*[@id=\"mysearch\"]")).sendKeys("325106");
  	Actions ac=new Actions(dr);
  	try {
  		Thread.sleep(7000);
  	}
  	catch(Exception e)
  	{}
  	ac.sendKeys(Keys.ENTER).build().perform();
  	try {
  		Thread.sleep(7000);
  	}
  	catch(Exception e)
  	{}
  	Actions ac1=new Actions(dr);
  	ac1.sendKeys(Keys.PAGE_DOWN).build().perform();
  	
  	dr.manage().window().maximize();
  	try {
  		Thread.sleep(5000);
  	}
  	catch(Exception e)
  	{}
  	System.out.println("in submit 1");
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='Project']//input")).sendKeys("Disa LR4");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  ac.sendKeys(Keys.ENTER).build().perform();
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='ASSIGNMENT_STATUS']//input")).sendKeys("Trainee");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  ac.sendKeys(Keys.ENTER).build().perform();
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='PERCENTAGE_ALLOCATION']//input")).sendKeys("100");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='BILLING_ROLE']//input")).sendKeys("Trainee Software Engineer");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  Select s=new Select(dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='ASSIGN_TYPE']//select")));
  s.selectByVisibleText("Onsite");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='LOCATION']//input")).sendKeys("Noida");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='Start_Date']//input")).sendKeys("2019-12-10");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='COMMENTS']//textarea")).sendKeys("No comments");
  try {
  	Thread.sleep(2000);
  }
  catch(Exception e)
  {}
  dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div[1]//div[@col-id='ASSIGNMENT_STATUS']//input")).sendKeys("Trainee");
  try {
	  	Thread.sleep(2000);
	  }
  catch(Exception e)
  {}
  dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[2]/button")).click();
  try {
  	   Thread.sleep(5000);
  	   }
  	   catch(Exception e)
  	   {
  		  
  	   }
  	   String s1=" ";
  	   SoftAssert sa=new SoftAssert();
  	   try {
  	   s1=dr.findElement(By.xpath("/html/body/modal-container/div/div/ng-component/div/form/div[2]/div/div[3]/div[1]/h2")).getText();
  	  
  	   sa.assertEquals(s1, "Ashok Kumar (325106)");
  	   sa.assertAll();
  	   }catch(Exception e)
  	   {
  		   sa.assertEquals(s1, "Ashok Kumar (325106)");
  		   sa.assertAll();
  	   }
  	   extent_report("TC_6_2", "To verify if user is able to navigate next window", "Test case passed", "Test case failed", s1, "Ashok Kumar (325106)");
  	   dr.findElement(By.xpath("/html/body/modal-container/div/div/ng-component/div/form/div[2]/div/div[3]/div[2]/div/div/button[2]")).click();
  	   try {
  		   Thread.sleep(5000);
  	   }
  	   catch(Exception e)
  	   {}
  	   dr.findElement(By.xpath("/html/body/modal-container/div/div/ng-component/div/form/div[2]/div/div[4]/div[2]/div/div/button[1]")).click();
  }
  
  
  
//test 7 
	
	

	@When("^Enter valid login credential and login in and hovering over requests and select supervison change and enter empid or emp name in new page$")
	public void enter_valid_login_credential_and_login_in_and_hovering_over_requests_and_select_supervison_change_and_enter_empid_or_emp_name_in_new_page() throws Throwable {
		 dr.findElement(By.name("username")).sendKeys("arun.pp");
		   dr.findElement(By.name("password")).sendKeys("Amritha@1234");
		   //dr.findElement(By.className("btn btn-primary btn-block btn-lg")).click();
		   dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();
		
		

		   WebDriverWait wait=new WebDriverWait(dr,20);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("dropdown-toggle")));
		   
		   
		   
		   Actions actions = new Actions(dr);
	        //Retrieve WebElement 'Music' to perform mouse hover 
	     WebElement menuOption = dr.findElement(By.className("dropdown-toggle"));
	     //Mouse hover menuOption 'Music'
	     actions.moveToElement(menuOption).perform();
		   
	     //WebElement subMenuOption = dr.findElement(By.linkText("Supervisor Change")); 
	     //Mouse hover menuOption 'Rock'
	     dr.findElement(By.linkText("Supervisor Change")).click();
	    // actions.moveToElement(subMenuOption).click();
	     
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mysearch")));
		   
		   
		   dr.findElement(By.id("mysearch")).sendKeys("1261");
		   //316557
		   
		  
		   dr.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
		dr.findElement(By.xpath("(//a[contains(@href, '#')])[3]")).click();
		   
		   
		   
		
		
		
	}

	@Then("^verify is able to navigate to change page$")
	public void verify_is_able_to_navigate_to_change_page() throws Throwable {
	 
		 WebDriverWait wait=new WebDriverWait(dr,20);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("avatar")));
		   
		   
		   
		   String url="https://ra-staging.globallogic.com/hractions/bulkrmchange";
		   
		   
		   String url2=dr.getCurrentUrl();
		   
		   
		   
		   SoftAssert as=new SoftAssert();
		   
		   as.assertEquals(url,url2);
		   as.assertAll();
		   
		   
		
	}

	@Then("^Then Display emp photo$")
	public void then_Display_emp_photo() throws Throwable {
	 
		SoftAssert as=new SoftAssert();
		//System.out.println(ExpectedConditions.visibilityOfElementLocated(By.className("avatkar")));
		//as.assertEquals(arg0, arg1);
		//as.assertAll(ExpectedConditions.visibilityOfElementLocated(By.className("avatar")));
	
	
			as.assertEquals(true,dr.findElement(By.className("avatar")).isDisplayed());
	as.assertAll();
	}

	@Then("^Then Display Designation$")
	public void then_Display_Designation() throws Throwable {
	/*	SoftAssert as=new SoftAssert();
as.assertEquals(true,dr.findElement(By.xpath("//label[contains(text(),'Designation:')]")).isDisplayed());
as.assertAll();*/
		
		SoftAssert as=new SoftAssert();
		as.assertEquals("Designation: Manager",dr.findElement(By.xpath("//p[contains(text(),'Manager')]")).getText());
		as.assertAll();
		
		
		
		
	}

	@Then("^Then Display Join date$")
	public void then_Display_Join_date() throws Throwable {
	   
		/*SoftAssert as=new SoftAssert();
		as.assertEquals(true,dr.findElement(By.xpath("//label[contains(text(),'Joining Date:')]")).isDisplayed());
		as.assertAll();*/
		
		
		
		
		SoftAssert as=new SoftAssert();
		as.assertEquals("Joining Date: 2006-08-10",dr.findElement(By.xpath("//p[contains(text(),'2006-08-10')]")).getText());
		as.assertAll();
		
		
	}

	@Then("^Then Display Supervison$")
	public void then_Display_Supervison() throws Throwable {
	
		SoftAssert as=new SoftAssert();
		as.assertEquals("Supervisor: Anurag Agrawal (3411) Change",dr.findElement(By.xpath("//p[contains(text(),'Anurag Agrawal (3411)')]")).getText());
		as.assertAll();
		
	}

	@Then("^Then Display clickeble change icon$")
	public void then_Display_clickeble_change_icon() throws Throwable {
		SoftAssert as=new SoftAssert();
		as.assertEquals(true,dr.findElement(By.xpath("//button[@class='badge change']")).isDisplayed());
		as.assertAll();
		
	}

	@Then("^Then Display Supervison change date$")
	public void then_Display_Supervison_change_date() throws Throwable {
		SoftAssert as=new SoftAssert();
		as.assertEquals(true,dr.findElement(By.xpath("//p[contains(text(),'2018-07-01')]")).isDisplayed());
		as.assertAll();
	}

	@Then("^Then Display Direct Reportees Grid$")
	public void then_Display_Direct_Reportees_Grid() throws Throwable {
		/*//ag-header-cell-text
		String s=dr.findElement(By.className("ag-header-cell-text")).getText();
		System.out.println(s);
		String s1="Employee Name";
		
		
		dr.findElement(By.className("ag-header-cell-text")).click();
		dr.findElement(By.className("ag-header-cell-text")).click();
		
		String name_exp_emp="Vishnu Kumar";
		String name_emp=dr.findElement(By.xpath("//span[contains(text(),'Vishnu Kumar')]")).getText();
		System.out.println(name_emp);
		SoftAssert as=new SoftAssert();
		as.assertEquals(name_exp_emp,name_emp);
		as.assertAll();
		*/
		SoftAssert as=new SoftAssert();
		try{
			
			dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[3]"));
			
			as.assertEquals(true,dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[3]")).isDisplayed());
			
			
			as.assertAll();
			
		}catch(Exception e) {
			
			as.assertEquals(true,false);
			as.assertAll();
			
		}
		
		
		
		
		
		
	  
	}

	@Then("^Then Display continue button$")
	public void then_Display_continue_button() throws Throwable {
		 WebDriverWait wait=new WebDriverWait(dr,20);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-primary wide ml-30 pull-right']")));
		   
	//dr.findElement(By.className("btn btn-primary wide ml-30 pull-right")).click();
		   SoftAssert as=new SoftAssert();
			as.assertEquals(true,dr.findElement(By.xpath("//button[@class='btn btn-primary wide ml-30 pull-right']")).isDisplayed());
			as.assertAll();
			
	
	
	}


 
	
	//testcase8
	
	@When("^Supervisor change request$")

	public void supervisor_change() throws Throwable{

	System.out.println("Supervisor");

	wait1 = new WebDriverWait(dr,20);
	wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ramenu\"]/li[2]/a")));

	WebElement we =dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/a"));

	Actions kb = new Actions(dr);
	kb.moveToElement(we).build().perform();

	dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[3]/a")).click();

	wait1 = new WebDriverWait(dr,20);
	wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mysearch\"]")));

	dr.findElement(By.xpath("//*[@id=\"mysearch\"]")).sendKeys("323113");

	try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}

	kb.sendKeys(Keys.ENTER).build().perform();

	wait1 = new WebDriverWait(dr,20);
	wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='project-overview rmChange']//p[3]//button")));

	dr.findElement(By.xpath("//div[@class='project-overview rmChange']//p[3]//button")).click();

	wait1 = new WebDriverWait(dr,20);
	wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='col-xs-3']//input")));

	dr.findElement(By.xpath("//div[@class='col-xs-3']//input")).sendKeys("325166");

	try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}

	kb.sendKeys(Keys.ENTER).build().perform();

	wait1 = new WebDriverWait(dr,20);
	wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='mat-input-infix mat-form-field-infix']//input")));


	dr.findElement(By.xpath("//div[@class='mat-input-infix mat-form-field-infix']//input")).sendKeys("2020-02-06");

	dr.findElement(By.xpath("//div[@class='modal-footer clearfix']//input")).click();

	try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}

	}
	
	
	@Then("^Success message$")

	public void success_message()throws Throwable{
		try {
			Thread.sleep(4000);
		}
		catch(Exception e)
		{}
	System.out.println("chlja bee");
	String act =dr.findElement(By.xpath("//div[@class='alert alert-success mt-5 mb-10 ng-star-inserted']")).getText();

	System.out.println(act);
	String exp = "Success : Change Supervisor request successfully processed.";

	//extent_report("Supervisor Change", "Success Message", "Title verified", "Title not verified", act, exp);

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Current Requests$")
	public void then_Current_Requests() throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='col-md-12']/h4")).getText();
	System.out.println(act);

	String exp = "Current Requests";

	//extent_report("Supervisor Change", "Current Request", "Title verified", "Title not verified", act, exp);

	 SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();
	   
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}

	}

	@Then("^Request Made By$")
	public void then_Request_Made_By() throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//th[1]")).getText();
	System.out.println(act);

	String exp = "Request Made By";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Request Made by", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Request Made By Value$")
	public void then_Request_Made_By_Value() throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//td[1]")).getText();
	System.out.println(act);

	String exp = "Rajat Kumar Agrawal (325108)";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Request value", "Value verified", "Value not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}

	}

	@Then("^Request Status$")
	public void then_Request_Status() throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//th[2]")).getText();
	System.out.println(act);

	        String exp = "Request Status";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();
	       
	  //      extent_report("Supervisor Change", "Request Status", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Request Status Value$")
	public void then_Request_Status_Value() throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//td[2]")).getText();
	System.out.println(act);

	String exp = "Approval Pending";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Status Vlaue", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Current Supervisor$")
	public void then_Current_Supervisor() throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//th[3]")).getText();
	System.out.println(act);

	String exp = "Current Supervisor";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Current Supervisor", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Current Supervisor Value$")
	public void then_Current_Supervisor_Value() throws Throwable {

	String act= dr.findElement(By.xpath("//div[@class='shadow mb-20']//td[3]")).getText();
	System.out.println(act);

	String exp = "Priyanshu Sharma (310480)";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Supervisor Value", "Value verified", "Value not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^New Supervisor$")
	public void then_New_Supervisor()throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//th[4]")).getText();
	System.out.println(act);

	String exp = "New Supervisor";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "New Supervisor", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^New Supervisor Value$")
	public void Then_New_Supervisor_Value()throws Throwable {

	String act= dr.findElement(By.xpath("//div[@class='shadow mb-20']//td[4]")).getText();
	System.out.println(act);

	String exp = "Vaibhav Sharma (325166)";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Supervisor Value", "Value verified", "Value not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Effective Date$")
	public void Effective_Date()throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//th[5]")).getText();
	System.out.println(act);

	String exp = "Effective Date";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Date", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}

	}

	@Then("^Effective Date Value$")
	public void Effective_Date_Value()throws Throwable {

	String act= dr.findElement(By.xpath("//div[@class='shadow mb-20']//td[5]")).getText();
	System.out.println(act);

	String exp = "2020-02-06";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Date value", "Value verified", "Value not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Approval Details$")
	public void Approval_Details()throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//th[6]")).getText();
	System.out.println(act);

	String exp = "Approval Details";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Approval Details", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Action$")
	public void Action()throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='shadow mb-20']//th[7]")).getText();
	System.out.println(act);

	String exp = "Action";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Action", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	@Then("^Close$")
	public void Close()throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='modal-footer clearfix']//button")).getText();
	System.out.println(act);

	String exp = "Close";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report("Supervisor Change", "Close", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}

	/*@Then("^Submit$")
	public void Submit()throws Throwable {

	String act = dr.findElement(By.xpath("//div[@class='modal-footer clearfix']//input")).getText();
	System.out.println(act);

	String exp = "Submit";

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	}*/

	@Then("^Disable$")
	public void Disable()throws Throwable {

	Boolean act = dr.findElement(By.xpath("//div[@class='modal-footer clearfix']//input")).isEnabled();
	System.out.println(act);

	Boolean exp = false;

	SoftAssert sa = new SoftAssert();
	 sa.assertEquals(act, exp);
	 sa.assertAll();

	//extent_report1("Supervisor Change", "Disable", "Title verified", "Title not verified", act, exp);
	 
	 try
	{Thread.sleep(3000);}
	catch(InterruptedException e)
	{ e.printStackTrace();}
	 
	 dr.close();
	}
	

	///test9
	
	@Given("^Home Page$")
	public void Logout() throws Throwable {
	 System.setProperty("webdriver.chrome.driver", "chromedriver_v78.exe");
	 dr =new ChromeDriver();
	 dr.manage().window().maximize();

	dr.get("https://ra-staging.globallogic.com/login");
	System.out.println("Login page is displayed");

	drw=new WebDriverWait(dr,20);
	drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")));
	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")).sendKeys("meghna.majhi");
	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[6]/div/input")).sendKeys("30041997Megh#");
	dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();


	}
	@When("^supervisor change$")
	public void supervisorChange() throws Throwable {
	dr.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	WebElement we1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/a"));
	Actions kb=new Actions(dr);
	kb.moveToElement(we1).build().perform();
	dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[3]")).click();
	dr.findElement(By.xpath("//*[@id=\"mysearch\"]")).sendKeys("Rajat Kumar");
	dr.findElement(By.xpath("//*[@id=\"imaginary_container\"]/div/type-ahead/typeahead-container/ul/li[1]/a")).click();


	try {

	  Thread.sleep(7000);
	  drw=new WebDriverWait(dr,5);
	  drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='tabA']/div/form/div/div[2]/alert/div/strong[1]")));
	

	}
	catch(Exception e) {
	dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[1]/div/div/div[2]/div/p[3]/span/button")).click();
	dr.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	//dr.switchTo().frame(0);
	dr.findElement(By.xpath("/html/body/modal-container/div/div/modal-content/div/form/div[4]/div[4]/div/type-ahead/input")).sendKeys("sanju dalla");
	dr.findElement(By.xpath("/html/body/modal-container/div/div/modal-content/div/form/div[4]/div[4]/div/type-ahead/typeahead-container/ul/li/a")).click();
	//dr.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
	dr.findElement(By.xpath("//div[@class='mat-input-infix mat-form-field-infix']/input")).sendKeys("2019-12-30");;

	//dr.findElement(By.xpath("//*[@id=\"mat-datepicker-2\"]/div[2]/mat-month-view/table/tbody/tr[5]/td[6]/div")).click();
	dr.findElement(By.xpath("/html/body/modal-container/div/div/modal-content/div/form/div[4]/div[6]/input")).click();
	try {
	 Thread.sleep(3000);
	 }
	 catch(Exception e1)
	 {}
	dr.findElement(By.xpath("/html/body/modal-container/div/div/modal-content/div/form/div[4]/div[7]/button")).click();
	}
	
	}

	@Then("^verify Change Supervisor Request Page$")
	public void verifyChangeSupervisorRequestPage() throws Throwable {
		System.out.println("Meghna");
	String er="Change Supervisor Request";
	String ar=dr.findElement(By.xpath("/html/body/user-app/supervisor-change/div/div/ul/li/a")).getText();

	SoftAssert sa1=new SoftAssert();
	sa1.assertEquals(er,ar);
	sa1.assertAll();

	}
	@Then("^verify Approval Request$")
	public void verifyApprovalRequest() throws Throwable {
		System.out.println("Meghna");
	String er="Approval of supervisor change requests are pending/waiting for oracle sync.";
	try {
	Thread.sleep(3000);
	} catch (Exception e) {
	// TODO: handle exception
	}
	String ar=dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/alert/div/strong[1]")).getText();
	SoftAssert sa2=new SoftAssert();
	sa2.assertEquals(er,ar);
	sa2.assertAll();

	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//test 10
	
	
	
	
	
	
	
	
	@When("^User enters the details to reaach at Supervisor Change Request$")
	public void user_enters_the_details_to_reaach_at_Supervisor_Change_Request() throws Throwable {
		dr.manage().window().maximize();
		 drw=new WebDriverWait(dr,20);
		 drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")));
		dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[5]/div/input")).sendKeys("rajat.kumar2");
		dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/div[6]/div/input")).sendKeys("Programming@853");
		dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();


		 Actions ac=new Actions(dr);
	       drw=new WebDriverWait(dr,20);
	       
		   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ramenu']/li[2]/a")));
	       we=dr.findElement(By.xpath("//*[@id='ramenu']/li[2]/a"));
	       ac.moveToElement(we).build().perform();
	       
	       
	       we=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/ul/li[3]/a"));
	       ac.moveToElement(we).click().build().perform();
	       
	       
		   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='mysearch']")));
	       we=dr.findElement(By.xpath("//*[@id=\"mysearch\"]"));
	       
	       we.sendKeys("325180");
	       
		   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"imaginary_container\"]/div/type-ahead/typeahead-container/ul/li/a/span")));
		   
		   we.sendKeys(Keys.ENTER);
		   
		   try {
			   Thread.sleep(5000);
		   drw=new WebDriverWait(dr,5);
		   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='tabA']/div/form/div/div[2]/alert/div/strong[1]")));
		  
		   we= drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']/ul[@id='ramenu']/li[1]/a")));
		    //dr.findElement(By.xpath("//div[@class='collapse navbar-collapse']/ul[@id='ramenu']/li[1]/a")).click();
		   we.click();
		   // System.out.println("Try");
		    
		   }
		   catch(Exception e) {
			   //System.out.println("No supervisor is assigned");
			   
			   drw=new WebDriverWait(dr,20);
			   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class ='project-overview rmChange']//button")));
			   dr.findElement(By.xpath("//*[@class ='project-overview rmChange']//button")).click();
			   
			   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@placeholder='Search an Employee']")));
			   dr.findElement(By.xpath("//*[@placeholder='Search an Employee']")).sendKeys("322403");
			   
			   we=drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='col-xs-3']/type-ahead/typeahead-container/ul/li")));
			   we.click();
			   
			   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@placeholder='Select date']")));

			   dr.findElement(By.xpath("//*[@placeholder='Select date']")).sendKeys("2019-12-22");
			   

			   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='modal-footer clearfix']/input")));
			   dr.findElement(By.xpath("//div[@class='modal-footer clearfix']/input")).click();
			   
			   
			   drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/modal-container/div/div/modal-content/div/form/div[1]/button/span")));
		       dr.findElement(By.xpath("/html/body/modal-container/div/div/modal-content/div/form/div[1]/button/span")).click();
	       
		       
		       try {
		    	   Thread.sleep(5000);
		       }
		       catch(Exception e1) {
		    	   e1.printStackTrace();
		       }
		       
		       
		       WebElement we1= drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']/ul[@id='ramenu']/li[1]/a")));		       
		       ac.moveToElement(we1).click().build().perform();
		       
			   
//		      System.out.println("Else Entered");			   
		   }                              
	}
	
	@Then("^Request made by the user$")
	public void request_made_by_the_user() throws Throwable {
	  
		try {
			Thread.sleep(3000);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		drw=new WebDriverWait(dr,20);
		WebElement we2=drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/user-app/ng-component/div/div[2]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/span/a")));
		String sup=we2.getText();
		
		we2.click();
  		//System.out.println(sup);
		
  	
  		try {
			Thread.sleep(3000);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
  		WebElement we3=drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/user-app/ng-component/div/ng-component/div/div/div/div/div/form/div/h4")));
  		String req=we3.getText();
  		String sub=req.substring(31,req.length()-1);
		//System.out.println(sub);
	
		sa.assertEquals(sup, sub);
		try
		{
			sa.assertAll();
		}
		catch(AssertionError e)
		{
			
		}
		
	}

	@Then("^Then Approval Pending$")
	public void approval_Pending() throws Throwable {
		Actions ac=new Actions(dr);
		try {
			Thread.sleep(3000);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		drw=new WebDriverWait(dr,20);
		drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@col-id='WorkflowStatus']")));
		al=(ArrayList<WebElement>)dr.findElements(By.xpath("//div[@col-id='WorkflowStatus']"));
		//System.out.println("ArrayList Size :  "+al.size());
		
		drw=new WebDriverWait(dr,20);
		drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@col-id='Name']")));
		al2=(ArrayList<WebElement>)dr.findElements(By.xpath("//div[@col-id='Name']"));
		//System.out.println("ArrayList2 Size :  "+al2.size());
		
		try {
			Thread.sleep(3000);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		WebElement element=drw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='aggrid']/div/div[4]/span[2]/button[3]")));
        ac.moveToElement(element).click().build().perform();		
        
		al.addAll((ArrayList<WebElement>)dr.findElements(By.xpath("//div[@col-id='WorkflowStatus']")));
		al2.addAll((ArrayList<WebElement>)dr.findElements(By.xpath("//div[@col-id='Name']")));
		
		
		for(int i=0;i<al.size();i++) {
			
			if(al.get(i).getText().equals("Approval Pending"))
				System.out.println(al2.get(i).getText());
			
		}
		
		
		
	}
	
	
}
