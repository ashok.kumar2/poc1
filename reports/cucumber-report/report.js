$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("a.feature");
formatter.feature({
  "line": 2,
  "name": "AUT_LOGIN",
  "description": "",
  "id": "aut-login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag1"
    }
  ]
});
formatter.scenario({
  "line": 7,
  "name": "Login with valid data",
  "description": "",
  "id": "aut-login;login-with-valid-data",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "login page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "on entering valid login crediatials and click sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Home page is displyed",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "Then Home page should contain Home",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Then Home page should contain Requests",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Then Home page should contain Reports",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Then Home page should contain Feedback",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Then Home page should contain Logged User Name",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Then Home page should contain Logged Pending Approvals",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Then Home page should contain Logged My Submissions",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "Then Home page should contain Logged My Historical Actions",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "Then Home page should contain Logged My Projects",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page()"
});
formatter.result({
  "duration": 6558024000,
  "status": "passed"
});
formatter.match({
  "location": "test1.on_entering_valid_login_crediatials_and_click_sign_in_button()"
});
formatter.result({
  "duration": 263759900,
  "status": "passed"
});
formatter.match({
  "location": "test1.home_page_is_displyed()"
});
formatter.result({
  "duration": 43500,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Home()"
});
formatter.result({
  "duration": 697914100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Requests()"
});
formatter.result({
  "duration": 84356200,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Reports()"
});
formatter.result({
  "duration": 63567700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Feedback()"
});
formatter.result({
  "duration": 76637100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_User_Name()"
});
formatter.result({
  "duration": 70586700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_Pending_Approvals()"
});
formatter.result({
  "duration": 57327900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_My_Submissions()"
});
formatter.result({
  "duration": 62350800,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_My_Historical_Actions()"
});
formatter.result({
  "duration": 56261400,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_My_Projects()"
});
formatter.result({
  "duration": 55475100,
  "status": "passed"
});
formatter.uri("b.feature");
formatter.feature({
  "line": 2,
  "name": "AUT_LOGIN1",
  "description": "",
  "id": "aut-login1",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag2"
    }
  ]
});
formatter.scenario({
  "line": 7,
  "name": "Login with valid data1",
  "description": "",
  "id": "aut-login1;login-with-valid-data1",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "login page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "on entering valid login crediatials and click sign in button and click on drop down and add  start and end date and click filter",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Result set is displyed in Awaiting Allocations Approvals",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "count of set should displyed in Awaiting Allocations Approvals",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "in case of no  pending reports mesage should dispalyed",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page()"
});
formatter.result({
  "duration": 6032864200,
  "status": "passed"
});
formatter.match({
  "location": "test1.on_entering_valid_login_crediatials_and_click_sign_in_button_and_click_on_drop_down_and_add_start_and_end_date_and_click_filter()"
});
formatter.result({
  "duration": 3317594500,
  "status": "passed"
});
formatter.match({
  "location": "test1.result_set_is_displyed_in_Awaiting_Allocations_Approvals()"
});
formatter.result({
  "duration": 5058657400,
  "status": "passed"
});
formatter.match({
  "location": "test1.count_of_set_should_displyed_in_Awaiting_Allocations_Approvals()"
});
formatter.result({
  "duration": 53599500,
  "status": "passed"
});
formatter.match({
  "location": "test1.in_case_of_no_pending_reports_mesage_should_dispalyed()"
});
formatter.result({
  "duration": 51856300,
  "status": "passed"
});
formatter.uri("c.feature");
formatter.feature({
  "line": 2,
  "name": "Employee Allocation",
  "description": "",
  "id": "employee-allocation",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag3"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "To assign Employye for a project",
  "description": "",
  "id": "employee-allocation;to-assign-employye-for-a-project",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "Dashboard page is displayed",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Employee assign request",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Approver page verify",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Click Button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Approve",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Reject",
  "keyword": "And "
});
formatter.match({
  "location": "test1.dashboard_page_is_displayed()"
});
formatter.result({
  "duration": 16250582100,
  "status": "passed"
});
formatter.match({
  "location": "test1.employee_assign()"
});
formatter.result({
  "duration": 103553452200,
  "status": "passed"
});
formatter.match({
  "location": "test1.Approver_page()"
});
formatter.result({
  "duration": 3045151700,
  "status": "passed"
});
formatter.match({
  "location": "test1.click_button()"
});
formatter.result({
  "duration": 3037715600,
  "status": "passed"
});
formatter.match({
  "location": "test1.approve()"
});
formatter.result({
  "duration": 3036445300,
  "status": "passed"
});
formatter.match({
  "location": "test1.reject()"
});
formatter.result({
  "duration": 3119328300,
  "status": "passed"
});
formatter.uri("d.feature");
formatter.feature({
  "line": 2,
  "name": "AUT  login",
  "description": "",
  "id": "aut--login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag4"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "To verify login for valid data",
  "description": "",
  "id": "aut--login;to-verify-login-for-valid-data",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "Login page is displayed",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User enters login detail",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Home page is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "verify projectName",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "verify startDate",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "verify endDate",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "verify customerName",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "verify businessUnit",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "verify shadowAllocationAllowed",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "verify allocationsforMonth",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Verify pending approvals",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page_is_displayed()"
});
formatter.result({
  "duration": 7046214300,
  "status": "passed"
});
formatter.match({
  "location": "test1.user_enters_login_details()"
});
formatter.result({
  "duration": 316267400,
  "status": "passed"
});
formatter.match({
  "location": "test1.home_page_is_displayed()"
});
formatter.result({
  "duration": 1218669000,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyprojectName()"
});
formatter.result({
  "duration": 846494400,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyStartDate()"
});
formatter.result({
  "duration": 28511500,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyEndDate()"
});
formatter.result({
  "duration": 26615500,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyCustomerName()"
});
formatter.result({
  "duration": 31071800,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyBusinessUnit()"
});
formatter.result({
  "duration": 26514700,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyShadowAllocationAllowed()"
});
formatter.result({
  "duration": 28536700,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyAllocationsforMonth()"
});
formatter.result({
  "duration": 3088638300,
  "error_message": "java.lang.AssertionError: The following asserts failed:\n\texpected [Dec, 2019] but found [Nov, 2019]\r\n\tat org.testng.asserts.SoftAssert.assertAll(SoftAssert.java:43)\r\n\tat STEP_DEF.test1.verifyAllocationsforMonth(test1.java:852)\r\n\tat ✽.And verify allocationsforMonth(d.feature:15)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "test1.VerifyPendingApprovals()"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("e.feature");
formatter.feature({
  "line": 2,
  "name": "AUT Login",
  "description": "",
  "id": "aut-login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag5"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "TC_5",
  "description": "",
  "id": "aut-login;tc-5",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "Login Page is displayed",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "User enters the details",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Employee Profile Photo",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Then Employee Name",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Then Employee Designation",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Then Employee Joining Date",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Then Employee Supervisor",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page_is_displayed1()"
});
formatter.result({
  "duration": 5724493000,
  "status": "passed"
});
formatter.match({
  "location": "test1.user_enter_login_details()"
});
formatter.result({
  "duration": 3609745700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Employee_Profile_Photo()"
});
formatter.result({
  "duration": 3028848900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Employee_Name()"
});
formatter.result({
  "duration": 57323900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Employee_Designation()"
});
formatter.result({
  "duration": 50985200,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Employee_Joining_Date()"
});
formatter.result({
  "duration": 52496600,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Employee_Supervisor()"
});
formatter.result({
  "duration": 49190700,
  "status": "passed"
});
formatter.uri("f.feature");
formatter.feature({
  "line": 2,
  "name": "AUT New Resource Alocation",
  "description": "",
  "id": "aut-new-resource-alocation",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag6"
    }
  ]
});
formatter.scenario({
  "line": 3,
  "name": "Allocation of Resource Employeewise",
  "description": "",
  "id": "aut-new-resource-alocation;allocation-of-resource-employeewise",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Login page is displayed",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Employee enter employeewise page details",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "verify add row button",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "Then able to navigate to next window",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page_is_displayed()"
});
formatter.result({
  "duration": 6842775500,
  "status": "passed"
});
formatter.match({
  "location": "test1.employee_enter_employeewise_page_details()"
});
formatter.result({
  "duration": 28776636500,
  "status": "passed"
});
formatter.match({
  "location": "test1.verify_add_row_button()"
});
formatter.result({
  "duration": 14160920000,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_able_to_navigate_to_next_window()"
});
formatter.result({
  "duration": 62073866200,
  "error_message": "java.lang.AssertionError: The following asserts failed:\n\texpected [Ashok Kumar (325106)] but found [ ]\r\n\tat org.testng.asserts.SoftAssert.assertAll(SoftAssert.java:43)\r\n\tat STEP_DEF.test1.then_able_to_navigate_to_next_window(test1.java:1190)\r\n\tat ✽.And Then able to navigate to next window(f.feature:7)\r\n",
  "status": "failed"
});
formatter.uri("g.feature");
formatter.feature({
  "line": 2,
  "name": "AUT_LOGIN_SUP",
  "description": "",
  "id": "aut-login-sup",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag7"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Login with valid data  and change sup",
  "description": "",
  "id": "aut-login-sup;login-with-valid-data--and-change-sup",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Enter valid login credential and login in and hovering over requests and select supervison change and enter empid or emp name in new page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "verify is able to navigate to change page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Then Display emp photo",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Then Display Designation",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Then Display Join date",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Then Display Supervison",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Then Display clickeble change icon",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Then Display Supervison change date",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Then Display Direct Reportees Grid",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Then Display continue button",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page()"
});
formatter.result({
  "duration": 14083389300,
  "status": "passed"
});
formatter.match({
  "location": "test1.enter_valid_login_credential_and_login_in_and_hovering_over_requests_and_select_supervison_change_and_enter_empid_or_emp_name_in_new_page()"
});
formatter.result({
  "duration": 10310137300,
  "status": "passed"
});
formatter.match({
  "location": "test1.verify_is_able_to_navigate_to_change_page()"
});
formatter.result({
  "duration": 1004138600,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_emp_photo()"
});
formatter.result({
  "duration": 29684500,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Designation()"
});
formatter.result({
  "duration": 31425400,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Join_date()"
});
formatter.result({
  "duration": 27191400,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Supervison()"
});
formatter.result({
  "duration": 25707500,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_clickeble_change_icon()"
});
formatter.result({
  "duration": 33494000,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Supervison_change_date()"
});
formatter.result({
  "duration": 31545100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Direct_Reportees_Grid()"
});
formatter.result({
  "duration": 38856900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_continue_button()"
});
formatter.result({
  "duration": 54206400,
  "status": "passed"
});
formatter.uri("h.feature");
formatter.feature({
  "line": 1,
  "name": "Resource Allocation",
  "description": "",
  "id": "resource-allocation",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "To verify login for valid data",
  "description": "",
  "id": "resource-allocation;to-verify-login-for-valid-data",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Tag8"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Dashboard page is displayed",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Supervisor change request",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Success message",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Current Requests",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Request Made By",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Request Made By Value",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Request Status",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Request Status Value",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Current Supervisor",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Current Supervisor Value",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "New Supervisor",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "New Supervisor Value",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "Effective Date",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "Effective Date Value",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "Approval Details",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "Action",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "Close",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "Disable",
  "keyword": "And "
});
formatter.match({
  "location": "test1.dashboard_page_is_displayed()"
});
formatter.result({
  "duration": 24095351300,
  "status": "passed"
});
formatter.match({
  "location": "test1.supervisor_change()"
});
formatter.result({
  "duration": 12504767200,
  "status": "passed"
});
formatter.match({
  "location": "test1.success_message()"
});
formatter.result({
  "duration": 7028350700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Current_Requests()"
});
formatter.result({
  "duration": 3029575900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Request_Made_By()"
});
formatter.result({
  "duration": 3027680600,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Request_Made_By_Value()"
});
formatter.result({
  "duration": 3026915600,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Request_Status()"
});
formatter.result({
  "duration": 3026356700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Request_Status_Value()"
});
formatter.result({
  "duration": 3025214100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Current_Supervisor()"
});
formatter.result({
  "duration": 3025489100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Current_Supervisor_Value()"
});
formatter.result({
  "duration": 3030173800,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_New_Supervisor()"
});
formatter.result({
  "duration": 3022856300,
  "status": "passed"
});
formatter.match({
  "location": "test1.Then_New_Supervisor_Value()"
});
formatter.result({
  "duration": 3024434200,
  "status": "passed"
});
formatter.match({
  "location": "test1.Effective_Date()"
});
formatter.result({
  "duration": 3026578700,
  "status": "passed"
});
formatter.match({
  "location": "test1.Effective_Date_Value()"
});
formatter.result({
  "duration": 3024186600,
  "status": "passed"
});
formatter.match({
  "location": "test1.Approval_Details()"
});
formatter.result({
  "duration": 3029320100,
  "status": "passed"
});
formatter.match({
  "location": "test1.Action()"
});
formatter.result({
  "duration": 3028004700,
  "status": "passed"
});
formatter.match({
  "location": "test1.Close()"
});
formatter.result({
  "duration": 3023060700,
  "status": "passed"
});
formatter.match({
  "location": "test1.Disable()"
});
formatter.result({
  "duration": 3136334300,
  "status": "passed"
});
formatter.uri("i.feature");
formatter.feature({
  "line": 2,
  "name": "AUT  login",
  "description": "",
  "id": "aut--login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag9"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "To verify supervisor change",
  "description": "",
  "id": "aut--login;to-verify-supervisor-change",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "supervisor change",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "verify Change Supervisor Request Page",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "verify Approval Request",
  "keyword": "And "
});
formatter.match({
  "location": "test1.Logout()"
});
formatter.result({
  "duration": 7141461700,
  "status": "passed"
});
formatter.match({
  "location": "test1.supervisorChange()"
});
formatter.result({
  "duration": 8431042300,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyChangeSupervisorRequestPage()"
});
formatter.result({
  "duration": 27193100,
  "status": "passed"
});
formatter.match({
  "location": "test1.verifyApprovalRequest()"
});
formatter.result({
  "duration": 3030937900,
  "status": "passed"
});
formatter.uri("j.feature");
formatter.feature({
  "line": 2,
  "name": "AUT  login",
  "description": "",
  "id": "aut--login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag10"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "TC_10",
  "description": "",
  "id": "aut--login;tc-10",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "Login Page is displayed",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "User enters the details to reaach at Supervisor Change Request",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Request made by the user",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Then Approval Pending",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page_is_displayed1()"
});
formatter.result({
  "duration": 6207812700,
  "status": "passed"
});
formatter.match({
  "location": "test1.user_enters_the_details_to_reaach_at_Supervisor_Change_Request()"
});
formatter.result({
  "duration": 8709475000,
  "status": "passed"
});
formatter.match({
  "location": "test1.request_made_by_the_user()"
});
formatter.result({
  "duration": 6172768800,
  "status": "passed"
});
formatter.match({
  "location": "test1.approval_Pending()"
});
formatter.result({
  "duration": 7693446600,
  "status": "passed"
});
});